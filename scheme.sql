-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.22-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных TASK_DB
DROP DATABASE IF EXISTS `TASK_DB`;

CREATE DATABASE IF NOT EXISTS `TASK_DB` /*!40100 DEFAULT CHARACTER SET utf8 */;
create user taskdb identified by 'taskdb';
grant select,update,insert,execute on TASK_DB.* to taskdb@'localhost' identified by 'taskdb';
USE `TASK_DB`;


-- Дамп структуры для таблица TASK_DB.client
CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `iin` varchar(12) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `mobile_number` varchar(50) NOT NULL,
  `birtdate` date NOT NULL,
  `sex` enum('M','F') NOT NULL,
  `id_num` varchar(20) NOT NULL,
  `id_issuer` enum('MJ','MVD') NOT NULL,
  `id_issue_date` date NOT NULL,
  `id_expire_date` date NOT NULL,
  `salary` int(11) NOT NULL,
  `com_expenses` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iin` (`iin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы TASK_DB.client: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
/*!40000 ALTER TABLE `client` ENABLE KEYS */;


-- Дамп структуры для таблица TASK_DB.loan_application
CREATE TABLE IF NOT EXISTS `loan_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` int(11) unsigned NOT NULL,
  `state` enum('CREATED','APPROVED','REJECTED','CANCELED') NOT NULL DEFAULT 'CREATED',
  `client_id` int(11) unsigned NOT NULL,
  `amount` float unsigned DEFAULT NULL,
  `term` int(11) unsigned DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `monthly_payment` float DEFAULT NULL,
  `amount_all_payments` float DEFAULT NULL,
  `amount_over_payments` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_loan_application_user` (`create_user`),
  KEY `FK_loan_application_client` (`client_id`),
  CONSTRAINT `FK_loan_application_client` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `FK_loan_application_user` FOREIGN KEY (`create_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы TASK_DB.loan_application: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `loan_application` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan_application` ENABLE KEYS */;


-- Дамп структуры для таблица TASK_DB.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы TASK_DB.user: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password_hash`, `fullname`) VALUES
	(1, 'btimur@gmail.com', '314d3c18c6f2b3f8e96181ff900235ea586c392bbb5382dcab96556ad25a23d2', 'Timur Berdygodzhayev 1');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
