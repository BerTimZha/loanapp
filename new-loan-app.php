<?php
include('model/LoanApplication.php');
session_start();
$config = include('config.php');

if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    die();
} 

$connection = new mysqli($config['db']['host'], $config['db']['user'], $config['db']['password'], $config['db']['alias']);
$user = User::findById($_SESSION['user_id'], $connection);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <script src="js/validate.js" type="text/javascript"></script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Кредитная заявка</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><?= $user->getFullname(); ?></a></li>
            <li><a href="logout.php">Выход</a></li>
        </ul>
    </div>
</nav>
<div class="container index-div1">
    <h3>Данные о клиенте</h3>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <form id="new-loan-app" action="new-loan-app-confirm.php" method="post">
                <div class="form-group">
                    <label for="iin">ИИН</label>
                    <input type="text" class="form-control" id="iin" name="iin" placeholder="ИИН" required>
                    <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
                </div>
                <div class="form-group">
                    <label for="lastname">Фамилия</label>
                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Фамилия"
                           required>
                    <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
                </div>
                <div class="form-group">
                    <label for="firstname">Имя</label>
                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Имя" required>
                    <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
                </div>
                <div class="form-group">
                    <label for="middlename">Отчество</label>
                    <input type="text" class="form-control" id="middlename" name="middlename" placeholder="Отчество">
                    <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
                </div>
                <div class="form-group">
                    <label for="mobile">Мобильный телефон</label>
                    <input type="tel" class="form-control" id="mobile" name="mobile"
                           placeholder="Мобильный телефон"
                           required>
                    <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
                </div>
                <div class="form-group">
                    <label for="birthdate">Дата рождения</label>
                    <input type="date" class="form-control" id="birthdate" name="birthdate" placeholder="Дата рождения"
                           required>
                    <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
                </div>
                <div class="form-group">
                    <label for="sex">Пол</label>
                    <input type="radio" id="sex_m" name="sex" value="M" checked>M
                    <input type="radio" id="sex_f" name="sex" value="F">Ж
                </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="id_num">Номер документа</label>
                <input type="text" class="form-control" id="id_num" name="id_num" placeholder="Номер документа"
                       required>
                <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
            </div>
            <div class="form-group">
                <label for="id_issuer">Кем выдан</label>
                <select id="id_issuer" name="id_issuer" class="form-control" required>
                    <option value="MJ">МЮ РК</option>
                    <option value="MVD">МВД РК</option>
                </select>
                <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
            </div>
            <div class="form-group">
                <label for="id_issue_date">Дата выдачи</label>
                <input type="date" class="form-control" id="id_issue_date" name="id_issue_date"
                       placeholder="Дата выдачи" required>
                <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
            </div>
            <div class="form-group">
                <label for="id_expire_date">Действителен до</label>
                <input type="date" class="form-control" id="id_expire_date" name="id_expire_date"
                       placeholder="Действителен до" required>
                <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
            </div>
            <div class="form-group">
                <label for="lastname">Заработная плата</label>
                <input type="number" class="form-control" id="salary" name="salary" placeholder="Заработная плата"
                       required>
                <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
            </div>
            <div class="form-group">
                <label for="com_expenses">Коммунальные платежи, аренда</label>
                <input type="number" class="form-control" id="com_expenses" name="com_expenses"
                       placeholder="Коммунальные платежи, аренда" required>
                <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
            </div>
            <div>
                <button type="button" class="btn btn-primary" onclick="confirm()">Подтвердить</button>
            </div>
        </div>
    </div>
    </form>
</div>
<script>


    $("#mobile").mask("+7(999)999-99-99"); 

	
    function validateClientFields(){
        clearErrorMessage();
		
        $(":input[required]:visible").each(function (index) {
            if ($(this).val().length == 0) {
                showErrorMessage($(this), $(this).attr('placeholder') + ' обязательное поле');
            }
        });
        $('#iin').each(function () { 
            if ($(this).val().length != 12) {
                showErrorMessage($(this), 'ИИН должен быть 12 символьным');
            }
        })
        $('#iin,#id_num,#salary,#com_expenses').each(function () {
            if (!patternOnlyNumbers.test($(this).val())) {
                showErrorMessage($(this), $(this).attr('placeholder') + ' должен быть цифровым');
            }
        })

        $('#firstname,#middlename,#lastname').each(function () {
            if (!patternOnlyAlphabet.test($(this).val())) {
                showErrorMessage($(this),$(this).attr('placeholder') + ' должен быть буквенным');
            }
        });
        $('#mobile').each(function () {
            if(!patternMobile.test($(this).val())){
                showErrorMessage($(this),'Номер мобильного телефона неверный');
            }
        });
        return $('.has-error').length===0; 
    }
    function confirm() { 

        if(validateClientFields()) { 
            $.getJSON("check-iin.json", {iin: $('#iin').val()}, function (data) { 
                if (data.result) {
                    $('#new-loan-app').submit();
                }else{
					alert("Не прошел валидацию ИИН");
				}
            });
        }
    }

</script>

</body>
