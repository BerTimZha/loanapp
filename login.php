<?php
/**
 * Created by PhpStorm.
 * User: TBERDYGOZHAYEV
 * Date: 05.08.2017
 * Time: 22:52
 */
    include('model/User.php');
    session_start();
    $config = include('config.php');
    $connection = new mysqli($config['db']['host'],$config['db']['user'],$config['db']['password'],$config['db']['alias']);
    if(isset($_SESSION['user_id'])){ 
        header("Location: index.php");
        die();
    }
    $errorMessage = false;
    if(isset($_POST['email'])){ 
        $user = User::findByUsername($_POST['email'],$connection); 
        if($user!=null && $user->getPasswordHash()==hash('sha256', $_POST['password'])){ 
                $_SESSION['user_id'] = $user->getId(); 
                header("Location: index.php"); 
                die();
        } else {
            $errorMessage = true;
        }

    }
?>
<!DOCTYPE html>
<html>
    <head>
	 <meta charset="utf-8">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container login-div1">
            <div class="login-form">
                <?php if($errorMessage) { ?>
                <div class="alert alert-danger" role="alert">
                    Имя пользователя или парол неверный
                </div>
                <?php } ?>
                <form action="login.php" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="email">email</label>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                            <input type="email" class="form-control" id="email" name="email" placeholder="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="email">password</label>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></div>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Вход</button>
                    </div>
                </form>

            </div>
        </div>
    </body>
</html>
