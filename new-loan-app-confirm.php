<?php

include('model/LoanApplication.php');
session_start();
$config = include('config.php');
if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    die();
}

if (!isset($_POST['iin'])) {
    header("Location: new-loan-app.php");
    die();
}
$connection = new mysqli($config['db']['host'], $config['db']['user'], $config['db']['password'], $config['db']['alias']);
$user = User::findById($_SESSION['user_id'], $connection);
$client = Client::findByIin($_POST['iin'], $connection); 
if ($client == null) {
    $client = new Client();
}

$client->setIin($_POST['iin']);
$client->setFirstname($_POST['firstname']);
$client->setMiddlename($_POST['middlename']);
$client->setLastname($_POST['lastname']);
$client->setMobileNumber($_POST['mobile_number']);
$client->setBirthdate($_POST['birthdate']);
$client->setSex($_POST['sex']);
$idCard = new IdCard();
$idCard->setNumber($_POST['id_num']);
$idCard->setIssuer($_POST['id_issuer']);
$idCard->setIssueDate($_POST['id_issue_date']);
$idCard->setExpireDate($_POST['id_expire_date']);
$client->setIdCard($idCard);
$client->setSalary($_POST['salary']);
$client->setComExpenses($_POST['com_expenses']);
$client->save($connection);    
$loanApp = new LoanApplication(); 
$loanApp->setClient($client);
$loanApp->setCreateUser($user);
$loanApp->save($connection); 
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/validate.js" type="text/javascript"></script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Кредитная заявка</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><?= $user->getFullname(); ?></a></li>
            <li><a href="logout.php">Выход</a></li>
        </ul>
    </div>
</nav>
<div class="container index-div1">
    <h3>Данные о кредите</h3>
    <form id="new-loan-app-confirm" action="new-loan-app-result.php" method="post">
        <input type="hidden" id="client_id" name="client_id" value="<?= $client->getId() ?>"/>
        <input type="hidden" id="id" name="id" value="<?= $loanApp->getId() ?>"/>
        <div class="form-group">
            <label for="amount">Сумма</label>
            <input type="number" class="form-control" id="amount" name="amount" placeholder="Сумма" required>
            <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
        </div>
        <div class="form-group">
            <label for="term">Срок</label>
            <select id="term" name="term" class="form-control" required>
                <option value="3">3</option>
                <option value="6">6</option>
                <option value="9">9</option>
                <option value="12">12</option>
                <option value="15">15</option>
                <option value="18">18</option>
                <option value="21">21</option>
                <option value="24">24</option>
            </select>
            <div class="alert alert-danger hide" role="alert" id="error-msg"></div>

        </div>
        <div class="form-group">
            <label for="rate">Ставка</label>
            <select id="rate" name="rate" class="form-control" required>
                <option value="9">9%</option>
                <option value="10">10%</option>
                <option value="11">11%</option>
            </select>
            <div class="alert alert-danger hide" role="alert" id="error-msg"></div>
        </div>
        <hr/>
        <div class="form-group">
            <label for="monthly_payment">Еж.платеж</label>
            <input type="number" class="form-control" id="monthly_payment" name="monthly_payment"
                   placeholder="Еж.платеж"
                   readonly>
        </div>
        <div class="form-group">
            <label for="amount_usd" class="label-usd">Сумма в $</label>
            <input type="number" class="form-control" id="amount_usd" name="amount_usd" placeholder="Сумма в $"
                   readonly>
        </div>
        <div class="form-group">
            <label for="amount_all_payments">Общая сумма выплат</label>
            <input type="number" class="form-control" id="amount_all_payments" name="amount_all_payments"
                   placeholder="Общая сумма выплат"
                   readonly>
        </div>
        <div class="form-group">
            <label for="amount_over_payments">Переплата</label>
            <input type="number" class="form-control" id="amount_over_payments" name="amount_over_payments"
                   placeholder="Переплата"
                   readonly>
        </div>

        <div>
            <button type="button" class="btn btn-primary" onclick="confirm()">Подтвердить</button>
            <button type="button" class="btn btn-default" onclick="cancel()">Отменить</button>
        </div>

    </form>
</div>
<script>
    var rates;
    $.getJSON("get-rates.php", function (data) {
        rates = data;
        $('.label-usd').html($('.label-usd').html() + ' (Курс ' + rates.USD + ')'); 
    });

    function validateLoanAppFields() { 
        clearErrorMessage();
        $(":input[required]:visible").each(function (index) {
            if ($(this).val().length == 0) {
                showErrorMessage($(this), $(this).attr('placeholder') + ' обязательное поле');
            }
        });
        $('#amount').each(function () {
            if (!patternOnlyNumbers.test($(this).val())) {
                showErrorMessage($(this), $(this).attr('placeholder') + ' должен быть цифровым');
            }
        })

        return $('.has-error').length === 0;
    }
    $('#amount').blur(function () { 
        if (validateLoanAppFields()) {
            calculate();

        }

    });
		
	$('#rate,#term').change(function () {
        if (validateLoanAppFields()) {
            calculate();

        }

    });
	
    function calculate() {
        var amount = $('#amount').val();
        var rate = $('#rate').val() / 1200;
        var term = $('#term').val();
        var payment = amount * rate / (1 - Math.pow(1 + rate, -1 * term));

        var amountAllPayments = payment * term;
        var amountOverPayments = amountAllPayments - amount;
        var paymentInUsd = amount / rates.USD;  
        $('#monthly_payment').val(payment.toFixed(2));

        $('#amount_all_payments').val(amountAllPayments.toFixed(2));
        $('#amount_over_payments').val(amountOverPayments.toFixed(2));

        $('#amount_usd').val(paymentInUsd.toFixed(2));

    }

    function confirm() {   
        if (validateLoanAppFields()) {
            calculate(); 
            $.getJSON("loan-app-save.php", $('#new-loan-app-confirm').serializeArray(), function (data) { // Сохранение данных кредитной заявки
            });
            $.getJSON("score-loan-app.json", {id: $('#id').val()}, function (data) { // Скооринг через запятую перечислить другие параметры
                if (data.result) {
                    $.getJSON("loan-app-save.php", {
                        id: $('#id').val(),
                        state: 'APPROVED'
                    }, function (data) {
                    });

                } else {
                    $.getJSON("loan-app-save.php", {
                        id: $('#id').val(),
                        state: 'REJECTED'
                    }, function (data) {
                    });
                }
                $(location).attr('href', 'loan-app-result.php?id='+$('#id').val()); 
            });
        }
    }
    function cancel() { 
        $.getJSON("loan-app-save.php", {id: $('#id').val(), state: 'CANCELED'}, function (data) {  
        });
        $(location).attr('href', 'index.php');
    }

</script>

</body>
