<?php

/**
 * Created by PhpStorm.
 * User: TBERDYGOZHAYEV
 * Date: 06.08.2017
 * Time: 23:01
 */
include('model/IdCard.php');

class Client
{
    private $id, $iin, $firstname, $middlename, $lastname, $mobileNumber, $birthdate, $sex;
    private $idCard, $salary, $comExpenses;
    private $fields;

    function  __construct()
    {
        $idCard = new IdCard();
    }

    public static function fillClient($row)
    {
        $client = new Client();
        $client->setId($row['id']);
        $client->setIin($row['iin']);
        $client->setFirstname($row['firstname']);
        $client->setMiddlename($row['middlename']);
        $client->setLastname($row['lastname']);
        $client->setMobileNumber($row['mobile_number']);
        $client->setBirthdate($row['birt_date']);
        $client->setSex($row['sex']);
        $client->setSalary($row['salary']);
        $client->setComExpenses($row['com_expenses']);
        $client->setIdCard(IdCard::fillIdCard($row));
        return $client;
    }

    public static function findById($id, $db)
    {
        $sql = "select * from client  where id=" . $id;
        $res = $db->query($sql);
        if ($res->data_seek(0)) {
            $row = $res->fetch_assoc();
            return self::fillClient($row);
        } else {
            return null;
        }
    }
    public static function findByIin($iin, $db)
    {
        $sql = "select * from client  where iin=" . $iin;
        $res = $db->query($sql);
        if ($res->data_seek(0)) {
            $row = $res->fetch_assoc();
            return self::fillClient($row);
        } else {
            return null;
        }
    }

    public function save($db){
        $this->fields['iin'] = $this->iin;
        $this->fields['firstname'] = $this->firstname;
        $this->fields['middlename'] = $this->middlename;
        $this->fields['lastname'] = $this->lastname;
        $this->fields['mobile_number'] = $this->mobileNumber;
        $this->fields['birtdate'] = $this->birthdate;
        $this->fields['sex'] = $this->sex;
        $this->fields['id_num'] = $this->idCard->getNumber();
        $this->fields['id_issuer'] = $this->idCard->getIssuer();
        $this->fields['id_issue_date'] = $this->idCard->getIssueDate();
        $this->fields['id_expire_date'] = $this->idCard->getExpireDate();
        $this->fields['salary'] = $this->salary;
        $this->fields['com_expenses'] = $this->comExpenses;
        if($this->id==null){
            $this->insert($db);
        } else {
            $this->update($db);
        }
    }

    private function insert($db){
        $f='';
        $v='';
        foreach ($this->fields as $field=>$value){
            $f.=','.$field;
            $v.=',\''.$value.'\'';
        }
        $sql = "insert into client(".substr($f,1).") values(".substr($v,1).")";
        $db->query($sql);
        $this->id = $db->insert_id;
    }
    private function update($db){
        $f='';
        foreach ($this->fields as $field=>$value){
            $f.=','.$field.'=\''.$value.'\'';
        }
        $sql = "update client set ".substr($f,1)." where id=".$this->id;
        $db->query($sql);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIin()
    {
        return $this->iin;
    }

    /**
     * @param mixed $iin
     */
    public function setIin($iin)
    {
        $this->iin = $iin;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * @param mixed $middlename
     */
    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    /**
     * @param mixed $mobileNumber
     */
    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;
    }

    /**
     * @return mixed
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param mixed $birthdate
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param mixed $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    /**
     * @return mixed
     */
    public function getIdCard()
    {
        return $this->idCard;
    }

    /**
     * @param mixed $idCard
     */
    public function setIdCard($idCard)
    {
        $this->idCard = $idCard;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param mixed $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     * @return mixed
     */
    public function getComExpenses()
    {
        return $this->comExpenses;
    }

    /**
     * @param mixed $comExpenses
     */
    public function setComExpenses($comExpenses)
    {
        $this->comExpenses = $comExpenses;
    }

}