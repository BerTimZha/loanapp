<?php

/**
 * Created by PhpStorm.
 * User: TBERDYGOZHAYEV
 * Date: 06.08.2017
 * Time: 23:06
 */
include('model/User.php');
include('model/Client.php');

class LoanApplication
{

    private $id, $createdAt, $createUser, $state, $client, $amount, $term, $rate;
    private $monthlyPayment, $amountAllPayments, $amountOverPayments;
    public static $STATES=['CREATED'=>'Создан','APPROVED'=>'Одобрен','REJECTED'=>'Отказан','CANCELED'=>'Отменен',];

    public static function fillLoanApplication($row, $db)
    {
        $la = new LoanApplication();
        $la->setId($row['id']);
        $la->setCreatedAt($row['created_at']);
        $la->setCreateUser(User::findById($row['create_user'], $db));
        $la->setState($row['state']);
        $la->setAmount($row['amount']);
        $la->setTerm($row['term']);
        $la->setMonthlyPayment($row['monthly_payment']);
        $la->setAmountAllPayments($row['amount_all_payments']);
        $la->setAmountOverPayments($row['amount_over_payments']);
        $la->setClient(Client::findById($row['client_id'], $db));
        return $la;
    }

    public static function findByIin($iin, $db)
    {
        $sql = "select la.* from loan_application la, client c where la.client_id=c.id and c.iin='" . $iin . "'";
        $res = $db->query($sql);
        $loanApps = array();
        if ($res->data_seek(0)) {
            while ($row = $res->fetch_assoc()) {
                $loanApps[] = self::fillLoanApplication($row, $db);
            }
            return $loanApps;
        } else {
            return null;
        }

    }

    public static function findById($id, $db)
    {
        $sql = "select la.* from loan_application la where la.id='" . $id . "'";
        $res = $db->query($sql);

        if ($res->data_seek(0)) {
            $row = $res->fetch_assoc();
            $loanApp = self::fillLoanApplication($row, $db);
            return $loanApp;
        } else {
            return null;
        }

    }

    public function save($db)
    {
        if ($this->createUser != null) {
            $this->fields['create_user'] = $this->createUser->getId();
        }
        if ($this->state != null) {
            $this->fields['state'] = $this->state;
        }
        if ($this->client != null) {
            $this->fields['client_id'] = $this->client->getId();
        }
        if ($this->amount != null) {
            $this->fields['amount'] = $this->amount;
        }
        if ($this->term != null) {
            $this->fields['term'] = $this->term;
        }
        if ($this->rate != null) {
            $this->fields['rate'] = $this->rate;
        }
        if ($this->monthlyPayment != null) {
            $this->fields['monthly_payment'] = $this->monthlyPayment;
        }
        if ($this->amountAllPayments != null) {
            $this->fields['amount_all_payments'] = $this->amountAllPayments;
        }
        if ($this->amountOverPayments != null) {
            $this->fields['amount_over_payments'] = $this->amountOverPayments;
        }
        if ($this->id == null) {
            $this->insert($db);
        } else {
            $this->update($db);
        }
    }

    private function insert($db)
    {
        $f = '';
        $v = '';
        foreach ($this->fields as $field => $value) {
            $f .= ',' . $field;
            $v .= ',\'' . $value . '\'';
        }
        $sql = "insert into loan_application(" . substr($f, 1) . ") values(" . substr($v, 1) . ")";
        $db->query($sql);
        $this->id = $db->insert_id;
    }

    private function update($db)
    {
        $f = '';
        foreach ($this->fields as $field => $value) {
            $f .= ',' . $field . '=\'' . $value . '\'';
        }
        $sql = "update loan_application set " . substr($f, 1) . " where id=" . $this->id;
        $db->query($sql);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCreateUser()
    {
        return $this->createUser;
    }

    /**
     * @param mixed $createUser
     */
    public function setCreateUser($createUser)
    {
        $this->createUser = $createUser;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @param mixed $term
     */
    public function setTerm($term)
    {
        $this->term = $term;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param mixed $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return mixed
     */
    public function getMonthlyPayment()
    {
        return $this->monthlyPayment;
    }

    /**
     * @param mixed $monthlyPayment
     */
    public function setMonthlyPayment($monthlyPayment)
    {
        $this->monthlyPayment = $monthlyPayment;
    }

    /**
     * @return mixed
     */
    public function getAmountAllPayments()
    {
        return $this->amountAllPayments;
    }

    /**
     * @param mixed $amountAllPayments
     */
    public function setAmountAllPayments($amountAllPayments)
    {
        $this->amountAllPayments = $amountAllPayments;
    }

    /**
     * @return mixed
     */
    public function getAmountOverPayments()
    {
        return $this->amountOverPayments;
    }

    /**
     * @param mixed $amountOverPayments
     */
    public function setAmountOverPayments($amountOverPayments)
    {
        $this->amountOverPayments = $amountOverPayments;
    }

    public function getStateName(){
        return LoanApplication::$STATES["$this->state"];
    }
}