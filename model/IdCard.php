<?php

/**
 * Created by PhpStorm.
 * User: TBERDYGOZHAYEV
 * Date: 06.08.2017
 * Time: 23:03
 */
class IdCard
{

    private $number,$issuer,$issueDate,$expireDate;


    public static  function fillIdCard($row){
        $idCard = new IdCard();
        $idCard->setNumber($row['id_num']);
        $idCard->setIssuer($row['id_issuer']);
        $idCard->setIssueDate($row['id_issue_date']);
        $idCard->setExpireDate($row['id_expire_date']);
        return $idCard;
    }
    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getIssuer()
    {
        return $this->issuer;
    }

    /**
     * @param mixed $issuer
     */
    public function setIssuer($issuer)
    {
        $this->issuer = $issuer;
    }

    /**
     * @return mixed
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    /**
     * @param mixed $issueDate
     */
    public function setIssueDate($issueDate)
    {
        $this->issueDate = $issueDate;
    }

    /**
     * @return mixed
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }

    /**
     * @param mixed $expireDate
     */
    public function setExpireDate($expireDate)
    {
        $this->expireDate = $expireDate;
    }

}