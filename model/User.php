<?php

/**
 * Created by PhpStorm.
 * User: TBERDYGOZHAYEV
 * Date: 06.08.2017
 * Time: 17:50
 */
class User
{
    private  $id,$username,$passwordHash,$fullname;
    function __construct()
    {
    }

    public static function fillUser($res){
        $user = new User();
        $user->setId($res['id']);
        $user->setUsername($res['username']);
        $user->setPasswordHash($res['password_hash']);
        $user->setFullname($res['fullname']);
        return $user;
    }

    public static function findByUsername($login,$db){
        $sql = "SELECT * FROM user where username='".$login."'";
        $res = $db->query($sql);
        if($res->data_seek(0)) {
            $row = $res->fetch_assoc();
            return User::fillUser($row);
        } else {
            return null;
        }

    }
    public static function findById($id,$db){
        $sql = "SELECT * FROM user where id='".$id."'";
        $res = $db->query($sql);
        if($res->data_seek(0)) {
            $row = $res->fetch_assoc();
            return User::fillUser($row);
        } else {
            return null;
        }

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param mixed $passwordHash
     */
    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;
    }

    /**
     * @return mixed
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param mixed $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

}