<?php
include('model/LoanApplication.php');
session_start();
$config = include('config.php');
if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    die();
}

$connection = new mysqli($config['db']['host'], $config['db']['user'], $config['db']['password'], $config['db']['alias']);

$user = User::findById($_SESSION['user_id'], $connection);

if (isset($_POST['iin'])) {
	
    $loanApps = LoanApplication::findByIin($_POST['iin'], $connection);
    if (!isset($loanApps)) {
		        header("Location: new-loan-app.php");
        die();
    }

}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Кредитная заявка</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><?= $user->getFullname(); ?></a></li>
            <li><a href="logout.php">Выход</a></li>
        </ul>
    </div>
</nav>
<div class="container index-div1">
    <form action="index.php" method="post">
        <div class="row">
            <div class="col-lg-12">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Введите ИИН клиента" id="iin" name="iin"
                           value="<?= (isset($_POST['iin']) ? $_POST['iin'] : '') ?>"
                            pattern="[0-9]{12}$" required>
                    <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"
                                                                    aria-hidden="true"></span></button>
                </div>
            </div>
        </div>
    </form>
	
	
    <?php if (isset($loanApps)) { ?>
        <br/>
        <div class="row">
            <div class="col-lg-12">
                <a href="new-loan-app.php" class="btn btn-primary">Новая заявка</a>
            </div>
            <br/>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-striped">
                    <tr>
                        <th>Дата обращения</th>
                        <th>Номер заявки</th>
                        <th>Сумма</th>
                        <th>Срок</th>
                        <th>Статус</th>
                    </tr>
                    <?php
                    foreach ($loanApps as &$loanApp) {   ?>
                        <tr>
                            <th><?= $loanApp->getCreatedAt() ?></th>
                            <th><?= $loanApp->getId() ?></th>
                            <th><?= $loanApp->getAmount() ?></th>
                            <th><?= $loanApp->getTerm() ?></th>
                            <th><?= $loanApp->getStateName() ?></th>
                        </tr>

                    <?php } ?>
                </table>

            </div>
        </div>
    <?php } ?>
</div>
</body>
