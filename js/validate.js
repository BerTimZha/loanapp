var patternOnlyNumbers = /^\d+$/;
var patternOnlyAlphabet = /^[а-яА-Я]*$/;
var patternMobile = /^\+7[(][701|702|777|747|707|771|778|700]{3}[)][0-9]{3}[-][0-9]{2}[-][0-9]{2}$/;

function clearErrorMessage() {
    $(".alert").each(function () {
        $(this).html('');
        $(this).addClass('hide');
    });
    $(".form-group").each(function () {
        $(this).removeClass('has-error');
    });
}
function showErrorMessage(input, message) {
    var divErrInput = $('.alert', input.parent());
    divErrInput.removeClass('hide');
    input.parent().addClass('has-error');
    divErrInput.html(divErrInput.html() + message + '<br/>');

}

