<?php
include('model/LoanApplication.php');
session_start();
$config = include('config.php');
if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    die();
}
$connection = new mysqli($config['db']['host'], $config['db']['user'], $config['db']['password'], $config['db']['alias']);
$loanApp = LoanApplication::findById($_GET['id'],$connection);
$loanApp->setAmount($_GET['amount']);
$loanApp->setTerm($_GET['term']);
$loanApp->setRate($_GET['rate']);
$loanApp->setMonthlyPayment($_GET['monthly_payment']);
$loanApp->setAmountAllPayments($_GET['amount_all_payments']);
$loanApp->setAmountOverPayments($_GET['amount_over_payments']);
$loanApp->setState($_GET['state']);
$loanApp->save($connection);
$result['result']=true;
json_encode($result); 