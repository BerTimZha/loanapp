<?php
include('model/LoanApplication.php');
session_start();
$config = include('config.php');
if(!isset($_SESSION['user_id'])){
    header("Location: login.php");
    die();
}
$connection = new mysqli($config['db']['host'],$config['db']['user'],$config['db']['password'],$config['db']['alias']);
$user = User::findById($_SESSION['user_id'],$connection);
$loanApp = LoanApplication::findById($_GET['id'],$connection);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Кредитная заявка</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><?= $user->getFullname();?></a></li>
            <li><a href="logout.php">Выход</a></li>
        </ul>
</nav>
<div class="container index-div1">
    <h3>Результат</h3>
    <div><?= $loanApp->getStateName() ?></div>
    <div><a href="index.php">Новый поиск</a> </div>
</div>

</body>
